import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import './Additional';
//import Addition from './Additional';

// Textbox component
function ShowBoxName(props){
  return(
  <div>
    <h3>{props.text}</h3>
  </div>
  )
}
class AddUser extends Component{
  constructor(props){
    super(props);
    this.state = { username: '', surname: '', age: '', 
          course: '', year: ''};
  }
  handleChange = ({ target}) => {
    this.setState({ [target.name]: target.value });
  }

  render(){
    return(
      <div>
      <form>
        <div>
            <ShowBoxName text={"Name"}/>
        <input 
          type="text"
          name="username"
          value={this.state.username}
          onChange={this.handleChange} 
        />
        </div>
        <div>
            <ShowBoxName text={"Surname"}/>
        <input 
          type="text"
          name="surname"
          value={this.state.surname}
          onChange={this.handleChange} 
        />
        </div>
        <div>
            <ShowBoxName text={"Age"}/>
        <input 
          type="text"
          name="age"
          value={this.state.age}
          onChange={this.handleChange} 
        />
        </div>
        <div>
            <ShowBoxName text={"Course"}/>
        <input 
          type="text"
          name="course"
          value={this.state.age}
          onChange={this.handleChange} 
        />
        </div>
        <div>
            <ShowBoxName text={"Year"}/>
        <input 
          type="text"
          name="year"
          value={this.state.age}
          onChange={this.handleChange} 
        />
        </div>
        
      </form>

      <h3>username is: {this.state.username}</h3>
      <h3>surname is: {this.state.surname}</h3>
      </div>
    );
  }
}

class App extends Component {
  render(){
    return(
      <div>
        <h1>Add person to Database</h1>
        <AddUser title="Add new person:"></AddUser>
      </div>
    );
  }
}
export default App;
